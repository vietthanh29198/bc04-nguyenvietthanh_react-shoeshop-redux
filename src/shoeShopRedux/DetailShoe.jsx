import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { image, name, price, description } = this.props.detailShoe;
    return (
      <div id="detailShoe" className="container my-2 p-5 border border-warning">
        <div className="row">
          <div className="col-4">
            <img src={image} alt="" className="w-100" />
          </div>
          <div className="col-8">
            <h3 className="text-primary">Tên: {name}</h3>
            <h4 className="text-danger">Giá: {price}$</h4>
            <h5>Mô tả: {description}</h5>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detailShoe: state.ShoeReducer.detailShoe,
  };
};

export default connect(mapStateToProps)(DetailShoe);
