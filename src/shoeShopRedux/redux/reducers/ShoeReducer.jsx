import { dataShoe } from "../../DataShoe";
import {
  GIAM_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG,
  TANG_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG,
  THEM_VAO_GIO_HANG,
  XEM_CHI_TIET,
  XOA_SAN_PHAM_TRONG_GIO_HANG,
} from "../constant/ShoeConstant";

const initialState = {
  shoeArr: dataShoe,
  detailShoe: dataShoe[0],
  gioHang: [],
};

export let ShoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case XEM_CHI_TIET: {
      state.detailShoe = action.payload;
      return { ...state };
    }
    case THEM_VAO_GIO_HANG: {
      let cloneGioHang = [...state.gioHang];
      let shoes = action.payload;
      let index = state.gioHang.findIndex((item) => {
        return item.id == shoes.id;
      });

      // th1: sản phẩm chưa có trong giỏ hàng
      if (index == -1) {
        let spGioHang = { ...shoes, soLuong: 1 };
        cloneGioHang.push(spGioHang);
      } else {
        // th2: sản phẩm đã có trong giỏ hàng

        // tăng số lượng sản phẩm lên 1 đơn vị
        cloneGioHang[index].soLuong++;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case XOA_SAN_PHAM_TRONG_GIO_HANG: {
      let cloneGioHang = [...state.gioHang];
      let idShoe = action.payload;
      let index = cloneGioHang.findIndex((item) => {
        return item.id == idShoe;
      });
      cloneGioHang.splice(index, 1);
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case TANG_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG: {
      let cloneGioHang = [...state.gioHang];
      let idShoe = action.payload;
      let index = cloneGioHang.findIndex((item) => {
        return item.id == idShoe;
      });
      cloneGioHang[index].soLuong++;
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case GIAM_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG: {
      let cloneGioHang = [...state.gioHang];
      let idShoe = action.payload;
      let index = cloneGioHang.findIndex((item) => {
        return item.id == idShoe;
      });
      cloneGioHang[index].soLuong--;
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
