import { ShoeReducer } from "./ShoeReducer";
import { combineReducers } from "redux";

export const rootReducer_shoeShopRedux = combineReducers({
  ShoeReducer,
});
