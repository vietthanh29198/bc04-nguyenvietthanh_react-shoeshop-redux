import React, { Component } from "react";
import { connect } from "react-redux";
import {
  GIAM_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG,
  TANG_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG,
  XOA_SAN_PHAM_TRONG_GIO_HANG,
} from "./redux/constant/ShoeConstant";

class GioHang extends Component {
  renderTbody = () => {
    let { gioHang } = this.props;
    return gioHang.map((item, index) => {
      return (
        <tr key={index.toString() + item.id}>
          <td className="text-primary">{item.name}</td>
          <td className="text-danger">{item.price}$</td>
          <td>
            <img src={item.image} alt="" style={{ width: 80 }} />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDecreaseNumber(item.id);
              }}
              className="btn btn-success"
            >
              -
            </button>
            <span className="mx-3">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleIncreaseNumber(item.id);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(item.id);
              }}
              className="btn btn-danger"
            >
              <i className="fa fa-trash"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    let { gioHang } = this.props;
    return (
      <div id="cartShoe" className="container py-5">
        {/* Giỏ hàng */}
        <table className="table table-bordered text-center">
          <thead className="table-dark">
            <tr>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody className="table-warning">{this.renderTbody()}</tbody>
        </table>

        {gioHang.length == 0 && (
          <p className="mt-5 text-center">Chưa có sản phẩm trong giỏ hàng</p>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.ShoeReducer.gioHang,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleRemove: (value) => {
      dispatch({
        type: XOA_SAN_PHAM_TRONG_GIO_HANG,
        payload: value,
      });
    },
    handleIncreaseNumber: (value) => {
      dispatch({
        type: TANG_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG,
        payload: value,
      });
    },
    handleDecreaseNumber: (value) => {
      dispatch({
        type: GIAM_SO_LUONG_SAN_PHAM_TRONG_GIO_HANG,
        payload: value,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GioHang);
