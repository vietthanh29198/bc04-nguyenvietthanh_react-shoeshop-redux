import { connect } from "react-redux";
import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  render() {
    let { shoeArr } = this.props;
    return (
      <div className="container">
        <div className="row">
          {shoeArr.map((item, index) => {
            return (
              <div
                className="col-3 border border-danger p-0  "
                key={index.toString() + item.id}
              >
                <ItemShoe
                  // handleAddToCart={this.props.handleAddToCart}
                  detail={item}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeArr: state.ShoeReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ListShoe);
