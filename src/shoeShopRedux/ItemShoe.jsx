import React, { Component, Fragment } from "react";
import { XEM_CHI_TIET, THEM_VAO_GIO_HANG } from "./redux/constant/ShoeConstant";
import { connect } from "react-redux";

class ItemShoe extends Component {
  render() {
    let { name, image } = this.props.detail;
    return (
      <Fragment>
        <div className="card" style={{ width: "100%", height: "100%" }}>
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <div className="row">
              <a href="#cartShoe" className="col-6">
                <button
                  onClick={() => {
                    this.props.handleThemVaoGioHang(this.props.detail);
                  }}
                  className="btn btn-danger h-100 w-100"
                >
                  Thêm vào giỏ hàng
                </button>
              </a>

              <a href="#detailShoe" className="col-6">
                <button
                  onClick={() => {
                    this.props.handleXemChiTiet(this.props.detail);
                  }}
                  className="btn btn-warning h-100 w-100"
                >
                  Xem chi tiết
                </button>
              </a>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleXemChiTiet: (value) => {
      dispatch({
        type: XEM_CHI_TIET,
        payload: value,
      });
    },
    handleThemVaoGioHang: (value) => {
      dispatch({
        type: THEM_VAO_GIO_HANG,
        payload: value,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
