import React, { Component } from "react";
import DetailShoe from "./DetailShoe";
import GioHang from "./GioHang";
import ListShoe from "./ListShoe";

export default class ShoeShopRedux extends Component {
  render() {
    return (
      <div>
        <GioHang />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
